# Word embeddings

This repository provides an implementation of *Wan2vec* and *Can2vec*.

#### Example
To run the software you should use the following command:<br/>
	``python src/main.py --input graph/EATnew.net --output emb/name.emb``

#### Options
You can check out the other options available using:<br/>
	``python src/main.py --help``

#### Requirements
``Gensim``, ``Networkx``, ``Numpy``, ``Sklearn``, ``Numpy``, ``Random`` and ``NLTK``
