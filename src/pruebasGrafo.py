import argparse
import numpy as np
import networkx as nx
from gensim.models import Word2Vec
from nltk.corpus import wordnet as wn

#G = nx.read_edgelist('/graph/EATnew.net', nodetype=int, data=(('weight',float),), create_using=nx.DiGraph())
G = nx.read_pajek('graph/EATnew.net')
DG =nx.DiGraph()

for edge in G.edges():
    pesoarista = G[edge[0]][edge[1]][0]["weight"]

    #print(edge[0] +" " + edge[1])
    #print("peso arista: "+ str(pesoarista))
    a = wn.synsets(edge[0])
    b = wn.synsets(edge[1])

    #print(edge[0])
    #print(a)
    #print(edge[1])
    #print(b)
    max_similarity = -1
    if len(a) != 0 and len(b) != 0:
        for w1 in a:
            for w2 in b:
                #a[0].path_similarity(b[0
                s = wn.path_similarity(w1,w2)

                if s is not None and s > max_similarity:
                    max_similarity = s
        print(edge[0] +" "+str(w1))
        print(edge[1] + " " + str(w2))
        print(max_similarity)


    weight = 0
    for n in G.neighbors(edge[0]):
        #print(G[edge[0]][n][0])
        weight += G[edge[0]][n][0]["weight"]

    pesoFin = 1 - (pesoarista/weight)
    DG.add_edge(edge[0],edge[1], weight=pesoFin)

    #print(edge[0])
    #print("pesoT "+ str(weight))
    #print("peso fin" + str(pesoFin))

    weigth = 0
    for n in G.neighbors(edge[1]):
        weight += G[edge[1]][n][0]["weight"]

    pesoFin = 1 - (pesoarista/weight)
    DG.add_edge(edge[1],edge[0], weight=pesoFin)
    #print(edge[1])
    #print("pesoT "+ str(weight))
    #print("peso fin" + str(pesoFin)+ "\n")
