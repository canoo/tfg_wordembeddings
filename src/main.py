'''
Reference implementation of node2vec.

Author: Aditya Grover

For more details, refer to the paper:
node2vec: Scalable Feature Learning for Networks
Aditya Grover and Jure Leskovec
Knowledge Discovery and Data Mining (KDD), 2016
'''

import argparse
import numpy as np
import networkx as nx
import node2vec
from gensim.models import Word2Vec
from nltk.corpus import wordnet as wn

def parse_args():
	'''
	Parses the node2vec arguments.
	'''
	parser = argparse.ArgumentParser(description="Run node2vec.")

	parser.add_argument('--input', nargs='?', default='graph/karate.edgelist',
	                    help='Input graph path')

	parser.add_argument('--output', nargs='?', default='emb/karate.emb',
	                    help='Embeddings path')

	parser.add_argument('--dimensions', type=int, default=128,
	                    help='Number of dimensions. Default is 128.')

	parser.add_argument('--walk-length', type=int, default=80,
	                    help='Length of walk per source. Default is 80.')

	parser.add_argument('--num-walks', type=int, default=10,
	                    help='Number of walks per source. Default is 10.')

	parser.add_argument('--window-size', type=int, default=10,
                    	help='Context size for optimization. Default is 10.')

	parser.add_argument('--iter', default=1, type=int,
                      help='Number of epochs in SGD')

	parser.add_argument('--workers', type=int, default=8,
	                    help='Number of parallel workers. Default is 8.')

	parser.add_argument('--p', type=float, default=1,
	                    help='Return hyperparameter. Default is 1.')

	parser.add_argument('--q', type=float, default=1,
	                    help='Inout hyperparameter. Default is 1.')

	parser.add_argument('--weighted', dest='weighted', action='store_true',
	                    help='Boolean specifying (un)weighted. Default is unweighted.')
	parser.add_argument('--unweighted', dest='unweighted', action='store_false')
	parser.set_defaults(weighted=False)

	parser.add_argument('--directed', dest='directed', action='store_true',
	                    help='Graph is (un)directed. Default is undirected.')

	parser.add_argument('--undirected', dest='undirected', action='store_false')

	parser.add_argument('--algorithm', type=int, default=1,
	                    help='Algorithm used to generate the model. 0 is Wan2vec, 1 is Can2vec')

	parser.add_argument('--wfunction', type=int, default=3,
	                    help='Function used to generate the graph. For Wan2vec: 0 = Frecuency, 1 = IAS, else = IF. For Can2vec: 0 = FF, else = FF + wup_similarity')

	parser.add_argument('--wupTresh', type=float, default=0.5, help='Treshold for wup_similarity')

	parser.set_defaults(directed=False)

	return parser.parse_args()

def read_graph():
	'''
	Reads the input network in networkx.
	'''
	G = nx.read_pajek('graph/EATnew.net')

	if args.algorithm == 0:
		G = getWan2vecGraph(G)
	else:
		G = getCan2vecGraph(G)

	print(str(G.number_of_nodes()))
	print(str(G.number_of_edges()))
	return G



def getWan2vecGraph(G):
	DG =nx.DiGraph()

	for edge in G.edges():
		pesoarista = G[edge[0]][edge[1]][0]["weight"]

		#print(edge[0] +" " + edge[1])
		#print("peso arista: "+ str(pesoarista))

		weight = 0
		for n in G.neighbors(edge[0]):
			#print(G[edge[0]][n][0])
			weight += G[edge[0]][n][0]["weight"]

		pesoFin = wan2vecOper(pesoarista,weight)
		DG.add_edge(edge[0],edge[1], weight=pesoFin)

		#print(edge[0])
		#print("pesoT "+ str(weight))
		#print("peso fin" + str(pesoFin))

		weigth = 0
		for n in G.neighbors(edge[1]):
			weight += G[edge[1]][n][0]["weight"]

		pesoFin = wan2vecOper(pesoarista,weight)
		DG.add_edge(edge[1],edge[0], weight=pesoFin)
		#print(edge[1])
		#print("pesoT "+ str(weight))
		#print("peso fin" + str(pesoFin)+ "\n")

	return DG


def wan2vecOper(pesoarista,pesoTotal):
	if args.wfunction == 0: #Frecuencia
		return pesoarista
	elif args.wfunction == 1: #IAS
		return pesoTotal - pesoarista
	else:#IF
		return 1 - (pesoarista/pesoTotal)


def getCan2vecGraph(G):
	DG =nx.DiGraph()

	for edge in G.edges():
		pesoarista = G[edge[0]][edge[1]][0]["weight"]
		pesoFin = pesoarista

		if pesoFin > 1:

			if edge[0] in DG.nodes() and edge[1] in DG.neighbors(edge[0]):
				p0 = DG[edge[0]][edge[1]]["weight"]
				DG[edge[0]][edge[1]]["weight"] = pesoFin + p0
			else:
				DG.add_edge(edge[0],edge[1], weight=pesoFin)

			if edge[0] in DG.neighbors(edge[1]):
				p0 = DG[edge[1]][edge[0]]["weight"]
				DG[edge[1]][edge[0]]["weight"] = pesoFin + p0
			else:
				DG.add_edge(edge[1],edge[0], weight=pesoFin)


	#Ahora se completa el grafo utilizando Sinonimos de wordnet
	if args.wfunction != 0:
		for n in DG.nodes():

			sinonimos = set()

			for synset in wn.synsets(n):
				for lemma in synset.lemmas():
					sinonimos.add(lemma.name())
			sinonimos = list(sinonimos)

			for s in sinonimos:

				if s not in DG.nodes():
					continue

				lista = []
				listaMod= []
				for n2 in DG.neighbors(n):

					max_similarity = wup_similarity(s,n2)

					if max_similarity != -1 or max_similarity> args.wupTresh:
						f = DG[n][n2]["weight"]

						if n2 not in DG.neighbors(s):
							#DG.add_edge(s,n2,weight=f)
							#DG.add_edge(n2,s,weight=f)
							lista.append((s,n2,f))
							lista.append((n2,s,f))

						elif DG[s][n2]["weight"] < f:
							#DG[s][n2]["weight"] = f
							#DG[n2][s]["weight"] = f
							listaMod.append((s,n2,f))
							listaMod.append((n2,s,f))

				for i in lista:
					DG.add_edge(i[0],i[1],weight=i[2])
				for i in listaMod:
					DG[i[0]][i[1]]["weight"] = i[2]

	return DG

def wup_similarity(w1,w2):
	a = wn.synsets(str(w1))
	b = wn.synsets(str(w2))

	if len(a) == 0 or len(b) == 0:
		return -1

	max_similarity = -1
	for w1 in a:
		for w2 in b:
			#print(edge[0] +" " + edge[1])
			#s = wn.path_similarity(w1,w2)
			s = wn.wup_similarity(w1,w2)
			if s is not None and s > max_similarity:
				max_similarity = s

	return max_similarity

def learn_embeddings(walks):
	'''
	Learn embeddings by optimizing the Skipgram objective using SGD.
	'''
	walks = [map(correctWords, walk) for walk in walks]
	model = Word2Vec(walks, size=args.dimensions, window=args.window_size, min_count=0, sg=1, workers=args.workers, iter=args.iter)
	model.wv.save_word2vec_format(args.output)
	return

def correctWords(word):
	return word.lower().strip().replace(" ", "_")

def main(args):
	'''
	Pipeline for representational learning for all nodes in a graph.
	'''
	nx_G = read_graph()
	G = node2vec.Graph(nx_G, args.directed, args.p, args.q)
	G.preprocess_transition_probs()
	walks = G.simulate_walks(args.num_walks, args.walk_length)
	learn_embeddings(walks)

if __name__ == "__main__":
	args = parse_args()
	main(args)
